<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'her');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V#uRf<AT#Fl4Jc~-BN)PfWi~+&<dtVPwNRjgE^WJdE(#@>+Z/K!lqOjaWF&Y6>Am');
define('SECURE_AUTH_KEY',  '+!Hto[N7(A`)n)+a&)oypj!e1 @mZsucED6vs<}E]]}5Aw~Wwp!mv&b<C--IH`c$');
define('LOGGED_IN_KEY',    '+tcFdVEsJ9SH{QhOD,Rl-_y6#fJvOuo,tDD3&btJ?Q4ZxrWmi !x{L5jc|%o]v!<');
define('NONCE_KEY',        '+:6+]XH&FJct({}Fq/@K4zaGr$(;<wj|L<E-X;:v>Qzb2y^u_Q#Oomze1pL>hGtw');
define('AUTH_SALT',        '{rlgY)E%Y)$4S;7-U4hpNq8e.iOPeW:(|C+o`+^g$KSys2yhwO[0oY4 x*!iOw1J');
define('SECURE_AUTH_SALT', 'b+WsyaL<*h.cXH.glGMiN3Bj-BpWwTf D>B6g#ER@4);KkLg-C_G7j 5![tFuf3{');
define('LOGGED_IN_SALT',   '@DIuq|%k&f.BB|3@1qNyh+u_+Cw,fNNj7!mPE>=:.DWY*;2Qo<+[=9rYWN{qhM,M');
define('NONCE_SALT',       'VcwLX9]+~|tjwiV2GKIV.pOZy7XN!LY5co:,lfX4uq9,9eEJDC};w~-%Hz3/M6EV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define('WP_HOME','http://192.168.1.124/her/');
define('WP_SITEURL','http://192.168.1.124/her/');
