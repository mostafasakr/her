-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2014 at 04:58 PM
-- Server version: 5.5.38
-- PHP Version: 5.3.10-1ubuntu3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `her`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Mr WordPress', '', 'https://wordpress.org/', '', '2014-09-06 13:53:08', '2014-09-06 13:53:08', 'Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=196 ;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/her', 'yes'),
(2, 'home', 'http://localhost/her', 'yes'),
(3, 'blogname', 'Her', 'yes'),
(4, 'blogdescription', 'for her', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hawklovely@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:13:"sakr/sakr.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '0', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', '', 'no'),
(41, 'template', 'hawk', 'yes'),
(42, 'stylesheet', 'hawk', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '0', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '29630', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '1', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '0', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '0', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:3:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}i:3;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:1;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(82, 'uninstall_plugins', 'a:0:{}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '29630', 'yes'),
(89, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(90, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(91, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'sidebars_widgets', 'a:5:{s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:19:"wp_inactive_widgets";a:0:{}s:12:"left-sidebar";a:2:{i:0;s:12:"categories-3";i:1;s:7:"pages-2";}s:14:"footer-sidebar";a:1:{i:0;s:13:"text_widget-2";}s:13:"array_version";i:3;}', 'yes'),
(96, 'cron', 'a:5:{i:1410286461;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1410291000;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1410313997;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1410357302;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(98, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:57:"https://downloads.wordpress.org/release/wordpress-4.0.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:57:"https://downloads.wordpress.org/release/wordpress-4.0.zip";s:10:"no_content";s:68:"https://downloads.wordpress.org/release/wordpress-4.0-no-content.zip";s:11:"new_bundled";s:69:"https://downloads.wordpress.org/release/wordpress-4.0-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"4.0";s:7:"version";s:3:"4.0";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"3.8";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1410271074;s:15:"version_checked";s:3:"4.0";s:12:"translations";a:0:{}}', 'yes'),
(100, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1410271083;s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:2:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"3.0.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.3.0.2.zip";}s:9:"hello.php";O:8:"stdClass":6:{s:2:"id";s:4:"3564";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";}}}', 'yes'),
(103, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1410271087;s:7:"checked";a:4:{s:4:"hawk";s:3:"1.1";s:14:"twentyfourteen";s:3:"1.2";s:14:"twentythirteen";s:3:"1.3";s:12:"twentytwelve";s:3:"1.5";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(104, '_transient_random_seed', '0e55d8b09bc17f30c35633f22945405c', 'yes'),
(105, '_site_transient_timeout_browser_aa74ce38e4c52fd7ca531d9c22914b97', '1410616407', 'yes'),
(106, '_site_transient_browser_aa74ce38e4c52fd7ca531d9c22914b97', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"37.0.2062.94";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(107, 'can_compress_scripts', '0', 'yes'),
(126, '_transient_twentyfourteen_category_count', '1', 'yes'),
(127, '_site_transient_timeout_browser_fa06a7c9d42ae5b386feae5b01f48a3f', '1410616503', 'yes'),
(128, '_site_transient_browser_fa06a7c9d42ae5b386feae5b01f48a3f', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:7:"Firefox";s:7:"version";s:4:"32.0";s:10:"update_url";s:23:"http://www.firefox.com/";s:7:"img_src";s:50:"http://s.wordpress.org/images/browsers/firefox.png";s:11:"img_src_ssl";s:49:"https://wordpress.org/images/browsers/firefox.png";s:15:"current_version";s:2:"16";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(132, 'theme_mods_twentyfourteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1410027017;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(133, 'current_theme', 'Hawk', 'yes'),
(134, 'theme_mods_hawk', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:3:"hii";i:2;s:3:"top";i:0;}}', 'yes'),
(135, 'theme_switched', '', 'yes'),
(136, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(155, 'widget_pages', 'a:3:{i:1;a:0:{}i:2;a:3:{s:5:"title";s:11:"Recommended";s:6:"sortby";s:10:"post_title";s:7:"exclude";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(156, 'widget_calendar', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(157, 'widget_tag_cloud', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(158, 'widget_nav_menu', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(167, 'category_children', 'a:0:{}', 'yes'),
(177, '_transient_timeout_plugin_slugs', '1410341924', 'no'),
(178, '_transient_plugin_slugs', 'a:3:{i:0;s:19:"akismet/akismet.php";i:1;s:9:"hello.php";i:2;s:13:"sakr/sakr.php";}', 'no'),
(179, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1410206100', 'no'),
(180, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: name lookup timed out</p></div><div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: name lookup timed out</p></div><div class="rss-widget"><ul></ul></div>', 'no'),
(184, 'rewrite_rules', 'a:167:{s:11:"apparels/?$";s:28:"index.php?post_type=apparels";s:41:"apparels/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=apparels&feed=$matches[1]";s:36:"apparels/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=apparels&feed=$matches[1]";s:28:"apparels/page/([0-9]{1,})/?$";s:46:"index.php?post_type=apparels&paged=$matches[1]";s:8:"shoes/?$";s:25:"index.php?post_type=shoes";s:38:"shoes/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?post_type=shoes&feed=$matches[1]";s:33:"shoes/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?post_type=shoes&feed=$matches[1]";s:25:"shoes/page/([0-9]{1,})/?$";s:43:"index.php?post_type=shoes&paged=$matches[1]";s:14:"accessories/?$";s:31:"index.php?post_type=accessories";s:44:"accessories/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=accessories&feed=$matches[1]";s:39:"accessories/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=accessories&feed=$matches[1]";s:31:"accessories/page/([0-9]{1,})/?$";s:49:"index.php?post_type=accessories&paged=$matches[1]";s:10:"jewelry/?$";s:27:"index.php?post_type=jewelry";s:40:"jewelry/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=jewelry&feed=$matches[1]";s:35:"jewelry/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=jewelry&feed=$matches[1]";s:27:"jewelry/page/([0-9]{1,})/?$";s:45:"index.php?post_type=jewelry&paged=$matches[1]";s:14:"beauty_care/?$";s:31:"index.php?post_type=beauty_care";s:44:"beauty_care/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=beauty_care&feed=$matches[1]";s:39:"beauty_care/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=beauty_care&feed=$matches[1]";s:31:"beauty_care/page/([0-9]{1,})/?$";s:49:"index.php?post_type=beauty_care&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:36:"apparels/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"apparels/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"apparels/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"apparels/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"apparels/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"apparels/([^/]+)/trackback/?$";s:35:"index.php?apparels=$matches[1]&tb=1";s:49:"apparels/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?apparels=$matches[1]&feed=$matches[2]";s:44:"apparels/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?apparels=$matches[1]&feed=$matches[2]";s:37:"apparels/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?apparels=$matches[1]&paged=$matches[2]";s:44:"apparels/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?apparels=$matches[1]&cpage=$matches[2]";s:29:"apparels/([^/]+)(/[0-9]+)?/?$";s:47:"index.php?apparels=$matches[1]&page=$matches[2]";s:25:"apparels/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"apparels/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"apparels/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"apparels/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"apparels/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"shoes/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"shoes/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"shoes/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"shoes/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"shoes/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:26:"shoes/([^/]+)/trackback/?$";s:32:"index.php?shoes=$matches[1]&tb=1";s:46:"shoes/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?shoes=$matches[1]&feed=$matches[2]";s:41:"shoes/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?shoes=$matches[1]&feed=$matches[2]";s:34:"shoes/([^/]+)/page/?([0-9]{1,})/?$";s:45:"index.php?shoes=$matches[1]&paged=$matches[2]";s:41:"shoes/([^/]+)/comment-page-([0-9]{1,})/?$";s:45:"index.php?shoes=$matches[1]&cpage=$matches[2]";s:26:"shoes/([^/]+)(/[0-9]+)?/?$";s:44:"index.php?shoes=$matches[1]&page=$matches[2]";s:22:"shoes/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:32:"shoes/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:52:"shoes/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"shoes/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"shoes/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"accessories/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:49:"accessories/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:69:"accessories/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"accessories/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"accessories/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:32:"accessories/([^/]+)/trackback/?$";s:38:"index.php?accessories=$matches[1]&tb=1";s:52:"accessories/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?accessories=$matches[1]&feed=$matches[2]";s:47:"accessories/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?accessories=$matches[1]&feed=$matches[2]";s:40:"accessories/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?accessories=$matches[1]&paged=$matches[2]";s:47:"accessories/([^/]+)/comment-page-([0-9]{1,})/?$";s:51:"index.php?accessories=$matches[1]&cpage=$matches[2]";s:32:"accessories/([^/]+)(/[0-9]+)?/?$";s:50:"index.php?accessories=$matches[1]&page=$matches[2]";s:28:"accessories/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:38:"accessories/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:58:"accessories/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"accessories/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"accessories/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:35:"jewelry/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"jewelry/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"jewelry/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"jewelry/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"jewelry/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:28:"jewelry/([^/]+)/trackback/?$";s:34:"index.php?jewelry=$matches[1]&tb=1";s:48:"jewelry/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?jewelry=$matches[1]&feed=$matches[2]";s:43:"jewelry/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?jewelry=$matches[1]&feed=$matches[2]";s:36:"jewelry/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?jewelry=$matches[1]&paged=$matches[2]";s:43:"jewelry/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?jewelry=$matches[1]&cpage=$matches[2]";s:28:"jewelry/([^/]+)(/[0-9]+)?/?$";s:46:"index.php?jewelry=$matches[1]&page=$matches[2]";s:24:"jewelry/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"jewelry/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"jewelry/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"jewelry/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"jewelry/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"beauty_care/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:49:"beauty_care/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:69:"beauty_care/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"beauty_care/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"beauty_care/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:32:"beauty_care/([^/]+)/trackback/?$";s:38:"index.php?beauty_care=$matches[1]&tb=1";s:52:"beauty_care/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?beauty_care=$matches[1]&feed=$matches[2]";s:47:"beauty_care/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?beauty_care=$matches[1]&feed=$matches[2]";s:40:"beauty_care/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?beauty_care=$matches[1]&paged=$matches[2]";s:47:"beauty_care/([^/]+)/comment-page-([0-9]{1,})/?$";s:51:"index.php?beauty_care=$matches[1]&cpage=$matches[2]";s:32:"beauty_care/([^/]+)(/[0-9]+)?/?$";s:50:"index.php?beauty_care=$matches[1]&page=$matches[2]";s:28:"beauty_care/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:38:"beauty_care/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:58:"beauty_care/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"beauty_care/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"beauty_care/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(190, 'recently_activated', 'a:0:{}', 'yes'),
(191, 'widget_text_widget', 'a:2:{i:2;a:3:{s:5:"title";s:14:"Please Read ME";s:6:"w_text";s:377:"This website template has been designed by Free Website Templates for you, for free. You can replace all this text with your own text. You can remove any link to our website from this website template, you''re free to use this website template without linking back to us. If you''re having problems editing this website template, then don''t hesitate to ask for help on the Forum.";s:7:"address";s:145:"18th Floor, Lorem ipsum dolor\r\nAdipiscing Bldg., Quesqui vestibulum Avenue\r\nSamar Loop St., Business Park\r\nQuisque vestibulum, 6029\r\n+32-819-4560";}s:12:"_multiwidget";i:1;}', 'yes'),
(194, '_site_transient_timeout_theme_roots', '1410272877', 'yes'),
(195, '_site_transient_theme_roots', 'a:4:{s:4:"hawk";s:7:"/themes";s:14:"twentyfourteen";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";s:12:"twentytwelve";s:7:"/themes";}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=96 ;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1410027142:1'),
(4, 6, '_edit_last', '1'),
(5, 6, '_edit_lock', '1410027157:1'),
(6, 8, '_edit_last', '1'),
(7, 8, '_edit_lock', '1410027179:1'),
(8, 10, '_edit_last', '1'),
(9, 10, '_edit_lock', '1410027196:1'),
(10, 12, '_menu_item_type', 'post_type'),
(11, 12, '_menu_item_menu_item_parent', '0'),
(12, 12, '_menu_item_object_id', '10'),
(13, 12, '_menu_item_object', 'page'),
(14, 12, '_menu_item_target', ''),
(15, 12, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(16, 12, '_menu_item_xfn', ''),
(17, 12, '_menu_item_url', ''),
(19, 13, '_menu_item_type', 'post_type'),
(20, 13, '_menu_item_menu_item_parent', '0'),
(21, 13, '_menu_item_object_id', '8'),
(22, 13, '_menu_item_object', 'page'),
(23, 13, '_menu_item_target', ''),
(24, 13, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(25, 13, '_menu_item_xfn', ''),
(26, 13, '_menu_item_url', ''),
(28, 14, '_menu_item_type', 'post_type'),
(29, 14, '_menu_item_menu_item_parent', '0'),
(30, 14, '_menu_item_object_id', '6'),
(31, 14, '_menu_item_object', 'page'),
(32, 14, '_menu_item_target', ''),
(33, 14, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(34, 14, '_menu_item_xfn', ''),
(35, 14, '_menu_item_url', ''),
(37, 15, '_menu_item_type', 'post_type'),
(38, 15, '_menu_item_menu_item_parent', '0'),
(39, 15, '_menu_item_object_id', '4'),
(40, 15, '_menu_item_object', 'page'),
(41, 15, '_menu_item_target', ''),
(42, 15, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(43, 15, '_menu_item_xfn', ''),
(44, 15, '_menu_item_url', ''),
(46, 18, '_edit_last', '1'),
(47, 18, '_edit_lock', '1410254651:1'),
(48, 18, 'price', '100'),
(49, 20, '_wp_attached_file', '2014/09/apparel1.jpg'),
(50, 20, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel1.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(51, 18, '_thumbnail_id', '20'),
(52, 22, '_edit_last', '1'),
(53, 22, '_edit_lock', '1410084520:1'),
(54, 23, '_wp_attached_file', '2014/09/apparel2.jpg'),
(55, 23, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel2.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(56, 24, '_wp_attached_file', '2014/09/apparel3.jpg'),
(57, 24, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel3.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(58, 25, '_wp_attached_file', '2014/09/apparel4.jpg'),
(59, 25, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel4.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(60, 26, '_wp_attached_file', '2014/09/apparel5.jpg'),
(61, 26, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel5.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel5-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(62, 27, '_wp_attached_file', '2014/09/apparel6.jpg'),
(63, 27, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel6.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel6-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(64, 28, '_wp_attached_file', '2014/09/apparel7.jpg'),
(65, 28, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel7.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel7-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(66, 29, '_wp_attached_file', '2014/09/apparel8.jpg'),
(67, 29, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:170;s:6:"height";i:220;s:4:"file";s:20:"2014/09/apparel8.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"apparel8-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(68, 22, '_thumbnail_id', '23'),
(69, 31, '_edit_last', '1'),
(70, 31, '_edit_lock', '1410084586:1'),
(71, 31, '_thumbnail_id', '29'),
(72, 31, 'price', '100'),
(73, 33, '_thumbnail_id', '28'),
(74, 33, 'price', '1900'),
(75, 33, '_edit_last', '1'),
(76, 33, '_edit_lock', '1410084632:1'),
(77, 35, '_thumbnail_id', '27'),
(78, 35, 'price', '100'),
(79, 35, '_edit_last', '1'),
(80, 35, '_edit_lock', '1410084671:1'),
(81, 37, '_thumbnail_id', '26'),
(82, 37, '_edit_last', '1'),
(83, 37, '_edit_lock', '1410084710:1'),
(84, 39, '_thumbnail_id', '25'),
(85, 39, '_edit_last', '1'),
(86, 39, '_edit_lock', '1410084783:1'),
(87, 39, 'price', '100'),
(88, 41, '_edit_last', '1'),
(89, 41, '_edit_lock', '1410095647:1'),
(90, 41, '_thumbnail_id', '24'),
(91, 41, 'price', '100'),
(92, 43, '_edit_last', '1'),
(93, 43, '_edit_lock', '1410099835:1'),
(94, 43, '_thumbnail_id', '28'),
(95, 43, 'price', '100');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2014-09-06 13:53:08', '2014-09-06 13:53:08', 'Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2014-09-06 13:53:08', '2014-09-06 13:53:08', '', 0, 'http://localhost/her/?p=1', 0, 'post', '', 1),
(2, 1, '2014-09-06 13:53:08', '2014-09-06 13:53:08', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/her/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'open', 'open', '', 'sample-page', '', '', '2014-09-06 13:53:08', '2014-09-06 13:53:08', '', 0, 'http://localhost/her/?page_id=2', 0, 'page', '', 0),
(3, 1, '2014-09-06 13:53:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2014-09-06 13:53:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/her/?p=3', 0, 'post', '', 0),
(4, 1, '2014-09-06 18:14:40', '2014-09-06 18:14:40', '', 'New Arrival', '', 'publish', 'open', 'open', '', 'new-arrival', '', '', '2014-09-06 18:14:40', '2014-09-06 18:14:40', '', 0, 'http://localhost/her/?page_id=4', 0, 'page', '', 0),
(5, 1, '2014-09-06 18:14:40', '2014-09-06 18:14:40', '', 'New Arrival', '', 'inherit', 'open', 'open', '', '4-revision-v1', '', '', '2014-09-06 18:14:40', '2014-09-06 18:14:40', '', 4, 'http://localhost/her/?p=5', 0, 'revision', '', 0),
(6, 1, '2014-09-06 18:14:58', '2014-09-06 18:14:58', '', 'Apparel', '', 'publish', 'open', 'open', '', 'apparel', '', '', '2014-09-06 18:14:58', '2014-09-06 18:14:58', '', 0, 'http://localhost/her/?page_id=6', 0, 'page', '', 0),
(7, 1, '2014-09-06 18:14:58', '2014-09-06 18:14:58', '', 'Apparel', '', 'inherit', 'open', 'open', '', '6-revision-v1', '', '', '2014-09-06 18:14:58', '2014-09-06 18:14:58', '', 6, 'http://localhost/her/?p=7', 0, 'revision', '', 0),
(8, 1, '2014-09-06 18:15:17', '2014-09-06 18:15:17', '', 'Beaty Care', '', 'publish', 'open', 'open', '', 'beaty-care', '', '', '2014-09-06 18:15:17', '2014-09-06 18:15:17', '', 0, 'http://localhost/her/?page_id=8', 0, 'page', '', 0),
(9, 1, '2014-09-06 18:15:17', '2014-09-06 18:15:17', '', 'Beaty Care', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-09-06 18:15:17', '2014-09-06 18:15:17', '', 8, 'http://localhost/her/?p=9', 0, 'revision', '', 0),
(10, 1, '2014-09-06 18:15:35', '2014-09-06 18:15:35', '', 'Shoes', '', 'publish', 'open', 'open', '', 'shoes', '', '', '2014-09-06 18:15:35', '2014-09-06 18:15:35', '', 0, 'http://localhost/her/?page_id=10', 0, 'page', '', 0),
(11, 1, '2014-09-06 18:15:35', '2014-09-06 18:15:35', '', 'Shoes', '', 'inherit', 'open', 'open', '', '10-revision-v1', '', '', '2014-09-06 18:15:35', '2014-09-06 18:15:35', '', 10, 'http://localhost/her/?p=11', 0, 'revision', '', 0),
(12, 1, '2014-09-06 18:16:10', '2014-09-06 18:16:10', ' ', '', '', 'publish', 'open', 'open', '', '12', '', '', '2014-09-06 18:16:10', '2014-09-06 18:16:10', '', 0, 'http://localhost/her/?p=12', 4, 'nav_menu_item', '', 0),
(13, 1, '2014-09-06 18:16:10', '2014-09-06 18:16:10', ' ', '', '', 'publish', 'open', 'open', '', '13', '', '', '2014-09-06 18:16:10', '2014-09-06 18:16:10', '', 0, 'http://localhost/her/?p=13', 3, 'nav_menu_item', '', 0),
(14, 1, '2014-09-06 18:16:10', '2014-09-06 18:16:10', ' ', '', '', 'publish', 'open', 'open', '', '14', '', '', '2014-09-06 18:16:10', '2014-09-06 18:16:10', '', 0, 'http://localhost/her/?p=14', 2, 'nav_menu_item', '', 0),
(15, 1, '2014-09-06 18:16:10', '2014-09-06 18:16:10', ' ', '', '', 'publish', 'open', 'open', '', '15', '', '', '2014-09-06 18:16:10', '2014-09-06 18:16:10', '', 0, 'http://localhost/her/?p=15', 1, 'nav_menu_item', '', 0),
(16, 1, '2014-09-06 19:32:21', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2014-09-06 19:32:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/her/?post_type=jewelry&p=16', 0, 'jewelry', '', 0),
(17, 1, '2014-09-06 19:35:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2014-09-06 19:35:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/her/?post_type=apparels&p=17', 0, 'apparels', '', 0),
(18, 1, '2014-09-07 08:29:57', '2014-09-07 08:29:57', 'sakr hawk', 'hawk', '', 'publish', 'open', 'open', '', 'hawk', '', '', '2014-09-07 10:09:40', '2014-09-07 10:09:40', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=18', 0, 'apparels', '', 0),
(19, 1, '2014-09-07 08:29:57', '2014-09-07 08:29:57', 'sakr hawk', 'hawk', '', 'inherit', 'open', 'open', '', '18-revision-v1', '', '', '2014-09-07 08:29:57', '2014-09-07 08:29:57', '', 18, 'http://localhost/her/?p=19', 0, 'revision', '', 0),
(20, 1, '2014-09-07 10:06:44', '2014-09-07 10:06:44', '', 'apparel1', '', 'inherit', 'open', 'open', '', 'apparel1', '', '', '2014-09-07 10:06:44', '2014-09-07 10:06:44', '', 18, 'http://localhost/her/wp-content/uploads/2014/09/apparel1.jpg', 0, 'attachment', 'image/jpeg', 0),
(22, 1, '2014-09-07 10:10:56', '2014-09-07 10:10:56', '', 'Lorem Ipsum hi', '', 'publish', 'open', 'open', '', 'lorem-ipsum-hi', '', '', '2014-09-07 10:10:56', '2014-09-07 10:10:56', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=22', 0, 'apparels', '', 0),
(23, 1, '2014-09-07 10:10:37', '2014-09-07 10:10:37', '', 'apparel2', '', 'inherit', 'open', 'open', '', 'apparel2', '', '', '2014-09-07 10:10:37', '2014-09-07 10:10:37', '', 22, 'http://localhost/her/wp-content/uploads/2014/09/apparel2.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2014-09-07 10:10:37', '2014-09-07 10:10:37', '', 'apparel3', '', 'inherit', 'open', 'open', '', 'apparel3', '', '', '2014-09-07 10:10:37', '2014-09-07 10:10:37', '', 22, 'http://localhost/her/wp-content/uploads/2014/09/apparel3.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2014-09-07 10:10:38', '2014-09-07 10:10:38', '', 'apparel4', '', 'inherit', 'open', 'open', '', 'apparel4', '', '', '2014-09-07 10:10:38', '2014-09-07 10:10:38', '', 22, 'http://localhost/her/wp-content/uploads/2014/09/apparel4.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2014-09-07 10:10:38', '2014-09-07 10:10:38', '', 'apparel5', '', 'inherit', 'open', 'open', '', 'apparel5', '', '', '2014-09-07 10:10:38', '2014-09-07 10:10:38', '', 22, 'http://localhost/her/wp-content/uploads/2014/09/apparel5.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2014-09-07 10:10:39', '2014-09-07 10:10:39', '', 'apparel6', '', 'inherit', 'open', 'open', '', 'apparel6', '', '', '2014-09-07 10:10:39', '2014-09-07 10:10:39', '', 22, 'http://localhost/her/wp-content/uploads/2014/09/apparel6.jpg', 0, 'attachment', 'image/jpeg', 0),
(28, 1, '2014-09-07 10:10:39', '2014-09-07 10:10:39', '', 'apparel7', '', 'inherit', 'open', 'open', '', 'apparel7', '', '', '2014-09-07 10:10:39', '2014-09-07 10:10:39', '', 22, 'http://localhost/her/wp-content/uploads/2014/09/apparel7.jpg', 0, 'attachment', 'image/jpeg', 0),
(29, 1, '2014-09-07 10:10:40', '2014-09-07 10:10:40', '', 'apparel8', '', 'inherit', 'open', 'open', '', 'apparel8', '', '', '2014-09-07 10:10:40', '2014-09-07 10:10:40', '', 22, 'http://localhost/her/wp-content/uploads/2014/09/apparel8.jpg', 0, 'attachment', 'image/jpeg', 0),
(30, 1, '2014-09-07 10:10:56', '2014-09-07 10:10:56', '', 'Lorem Ipsum hi', '', 'inherit', 'open', 'open', '', '22-revision-v1', '', '', '2014-09-07 10:10:56', '2014-09-07 10:10:56', '', 22, 'http://localhost/her/22-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2014-09-07 10:12:06', '2014-09-07 10:12:06', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Blouses &amp; Shirts</a>', 'die', '', 'publish', 'open', 'open', '', 'die', '', '', '2014-09-07 10:12:06', '2014-09-07 10:12:06', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=31', 0, 'apparels', '', 0),
(32, 1, '2014-09-07 10:12:06', '2014-09-07 10:12:06', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Blouses &amp; Shirts</a>', 'die', '', 'inherit', 'open', 'open', '', '31-revision-v1', '', '', '2014-09-07 10:12:06', '2014-09-07 10:12:06', '', 31, 'http://localhost/her/31-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2014-09-07 10:12:47', '2014-09-07 10:12:47', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Dresses</a>', 'why', '', 'publish', 'open', 'open', '', 'why', '', '', '2014-09-07 10:12:47', '2014-09-07 10:12:47', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=33', 0, 'apparels', '', 0),
(34, 1, '2014-09-07 10:12:47', '2014-09-07 10:12:47', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Dresses</a>', 'why', '', 'inherit', 'open', 'open', '', '33-revision-v1', '', '', '2014-09-07 10:12:47', '2014-09-07 10:12:47', '', 33, 'http://localhost/her/33-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2014-09-07 10:13:25', '2014-09-07 10:13:25', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Knits</a>', 'Knits', '', 'publish', 'open', 'open', '', 'knits', '', '', '2014-09-07 10:13:25', '2014-09-07 10:13:25', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=35', 0, 'apparels', '', 0),
(36, 1, '2014-09-07 10:13:25', '2014-09-07 10:13:25', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Knits</a>', 'Knits', '', 'inherit', 'open', 'open', '', '35-revision-v1', '', '', '2014-09-07 10:13:25', '2014-09-07 10:13:25', '', 35, 'http://localhost/her/35-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2014-09-07 10:14:01', '2014-09-07 10:14:01', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Denims &amp; Jeans</a>', 'Denims & Jeans', '', 'publish', 'open', 'open', '', 'denims-jeans', '', '', '2014-09-07 10:14:01', '2014-09-07 10:14:01', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=37', 0, 'apparels', '', 0),
(38, 1, '2014-09-07 10:14:01', '2014-09-07 10:14:01', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Denims &amp; Jeans</a>', 'Denims & Jeans', '', 'inherit', 'open', 'open', '', '37-revision-v1', '', '', '2014-09-07 10:14:01', '2014-09-07 10:14:01', '', 37, 'http://localhost/her/37-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2014-09-07 10:15:21', '2014-09-07 10:15:21', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Pants</a>', 'Pants', '', 'publish', 'open', 'open', '', 'pants', '', '', '2014-09-07 10:15:21', '2014-09-07 10:15:21', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=39', 0, 'apparels', '', 0),
(40, 1, '2014-09-07 10:15:21', '2014-09-07 10:15:21', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Pants</a>', 'Pants', '', 'inherit', 'open', 'open', '', '39-revision-v1', '', '', '2014-09-07 10:15:21', '2014-09-07 10:15:21', '', 39, 'http://localhost/her/39-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2014-09-07 10:16:02', '2014-09-07 10:16:02', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Skirts</a>', 'Skirts', '', 'publish', 'open', 'open', '', 'skirts', '', '', '2014-09-07 10:16:02', '2014-09-07 10:16:02', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=41', 0, 'apparels', '', 0),
(42, 1, '2014-09-07 10:16:02', '2014-09-07 10:16:02', '<a href="file:///home/sakr/Downloads/herdesignswebtemplate/apparel.html#">Skirts</a>', 'Skirts', '', 'inherit', 'open', 'open', '', '41-revision-v1', '', '', '2014-09-07 10:16:02', '2014-09-07 10:16:02', '', 41, 'http://localhost/her/41-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2014-09-07 13:17:03', '2014-09-07 13:17:03', 'fly', 'fly', '', 'publish', 'open', 'open', '', 'fly', '', '', '2014-09-07 13:17:03', '2014-09-07 13:17:03', '', 0, 'http://localhost/her/?post_type=apparels&#038;p=43', 0, 'apparels', '', 0),
(44, 1, '2014-09-07 13:17:03', '2014-09-07 13:17:03', 'fly', 'fly', '', 'inherit', 'open', 'open', '', '43-revision-v1', '', '', '2014-09-07 13:17:03', '2014-09-07 13:17:03', '', 43, 'http://localhost/her/43-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'hi', 'hi', 0),
(3, 'Casual Tops', 'casual-tops', 0),
(4, 'Blouses &amp; Shirts', 'blouses-shirts', 0),
(5, 'Dresses', 'dresses', 0),
(6, 'Knits', 'knits', 0),
(7, 'Denims &amp; Jeans', 'denims-jeans', 0),
(8, 'Pants', 'pants', 0),
(9, 'Skirts', 'skirts', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 2, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(22, 3, 0),
(31, 4, 0),
(33, 5, 0),
(35, 6, 0),
(37, 7, 0),
(37, 8, 0),
(39, 9, 0),
(39, 10, 0),
(41, 11, 0),
(41, 12, 0),
(43, 6, 0),
(43, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'category', '', 0, 1),
(4, 4, 'category', '', 0, 1),
(5, 5, 'category', '', 0, 1),
(6, 6, 'category', '', 0, 2),
(7, 7, 'category', '', 0, 1),
(8, 7, 'post_tag', '', 0, 2),
(9, 8, 'category', '', 0, 1),
(10, 8, 'post_tag', '', 0, 1),
(11, 9, 'category', '', 0, 1),
(12, 9, 'post_tag', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:4:{s:64:"9bd3756a78e36387dec048c1e3749cc0422e56e723f98e3f9f25ab8feb874453";i:1411221205;s:64:"332580d88a26bbcaf6f172091af2e30b248d92e3ffe4f51cb3bd2042ba7b6f4b";i:1411221302;s:64:"1c23bb7239d28a344f7302c75e98b954a3965ddc6cd7492e2df45381e84e8817";i:1411236603;s:64:"6ca8dc39c50a2f94cbfbf82700c33e203c2a4985682e91f9e06dde02fad9c39a";i:1410335598;}'),
(15, 1, 'wp_dashboard_quick_press_last_post_id', '3'),
(16, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(17, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";}'),
(18, 1, 'nav_menu_recently_edited', '2'),
(19, 1, 'wp_user-settings', 'libraryContent=browse'),
(20, 1, 'wp_user-settings-time', '1410084466'),
(21, 1, 'meta-box-order_apparels', 'a:3:{s:4:"side";s:51:"submitdiv,categorydiv,tagsdiv-post_tag,postimagediv";s:6:"normal";s:69:"postexcerpt,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(22, 1, 'screen_layout_apparels', '2'),
(23, 1, 'closedpostboxes_apparels', 'a:0:{}'),
(24, 1, 'metaboxhidden_apparels', 'a:1:{i:0;s:7:"slugdiv";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BxI7gxY4oUuBOAk5nyaqqN8xyyLnrz1', 'admin', 'hawklovely@gmail.com', '', '2014-09-06 13:53:06', '', 0, 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
