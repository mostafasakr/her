<?php

/*
  Plugin Name: Sakr
  Description: sakr developed widgets
 */
/* Start Adding Functions Below this Line */
class text_widget extends WP_Widget{
    function __construct() {
        $id_base= 'text_widget';
        $name=__('Text Widget', 'wpb_widget_domain');
        $widget_options=  array(
            'description'=> __('widget with text and title input', 'wpb_widget_domain')
        );
        $control_options='';
        parent::__construct($id_base, $name, $widget_options, $control_options);
    }
    
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $text = $instance['w_text'];
        $address = $instance['address'];
        // before and after widget arguments are defined by themes
//	echo $args['before_widget'];
        echo '<div class="first">';
	if ( ! empty( $title ) )
	echo $args['before_title'] . '<h3>'.$title .'</h3>'. $args['after_title'];
	 
	// This is where you run the code and display the output
	echo '<p>'.$text.'</p>';
	echo '<h4>Address</h4>';
	echo '<p>'.$address.'</p>';
        echo '</div>';
//	echo $args['after_widget'];
	}
        
        public function form($instance) {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __( 'New title', 'wpb_widget_domain' );
            }
            if (isset($instance['w_text'])) {
                $text = $instance['w_text'];
            } else {
                $text = __( 'New Text', 'wpb_widget_domain' );
            }
            if (isset($instance['address'])) {
                $address = $instance['address'];
            } else {
                $address = __( '21 mahalla , cairo , egypt', 'wpb_widget_domain' );
            }
            ?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	<label for="<?php echo $this->get_field_id( 'w_text' ); ?>"><?php _e( 'Text:' ); ?></label>
        <textarea class="widefat" id="<?php echo $this->get_field_id( 'w_text' ); ?>" name="<?php echo $this->get_field_name( 'w_text' ); ?>" type="text" ><?php echo esc_attr( $text ); ?></textarea>
       <label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address:' ); ?></label>
        <textarea class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" type="text" ><?php echo $address ; ?></textarea>
	</p>
	<?php 
        }
        public function update($new_instance, $old_instance) {
            $instance = array();
            $instance['title'] = ( !empty($new_instance['title']))?strip_tags( $new_instance['title'] ) : '';
            $instance['w_text'] = ( !empty($new_instance['w_text']))?strip_tags( $new_instance['w_text'] ) : '';
            $instance['address'] = ( !empty($new_instance['address']))?strip_tags( $new_instance['address'] ) : '';
            return $instance;
        }
}

class text_url_widget extends WP_Widget{
    function __construct() {
        $id_base= 'text_url_widget';
        $name=__('Text URL Widget', 'wpb_widget_domain');
        $widget_options=  array(
            'description'=> __('widget with text , URL and title input', 'wpb_widget_domain')
        );
        $control_options='';
        parent::__construct($id_base, $name, $widget_options, $control_options);
    }
    
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $text = $instance['w_text'];
        $url= $instance['url'];
        // before and after widget arguments are defined by themes
//	echo $args['before_widget'];
        echo '<div>';
	if ( ! empty( $title ) )
	echo $args['before_title'] . '<h3>'.$title .'</h3>'. $args['after_title'];
	 
	// This is where you run the code and display the output
	echo '<p>'.$text.'</p>';
        $items = explode(',', $url);
        foreach ($items AS $item){
            $word = explode('+', $item);
            echo '<a href="'.$word[0].'">'.$word[1].'</a>';
        }
        echo '</div>';
//	echo $args['after_widget'];
	}
        
        public function form($instance) {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __( 'New title', 'wpb_widget_domain' );
            }
            if (isset($instance['w_text'])) {
                $text = $instance['w_text'];
            } else {
                $text = __( 'New Text', 'wpb_widget_domain' );
            }
            if (isset($instance['url'])) {
                $url = $instance['url'];
            } else {
                $url = __( 'www.example.com+ cairo ,', 'wpb_widget_domain' );
            }
            ?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	<label for="<?php echo $this->get_field_id( 'w_text' ); ?>"><?php _e( 'Text:' ); ?></label>
        <textarea class="widefat" id="<?php echo $this->get_field_id( 'w_text' ); ?>" name="<?php echo $this->get_field_name( 'w_text' ); ?>" type="text" ><?php echo esc_attr( $text ); ?></textarea>
       <label for="<?php echo $this->get_field_id( 'url' ); ?>"><?php _e( 'URLs:' ); ?></label>
        <textarea class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" type="text" ><?php echo $url ; ?></textarea>
        add URLs seperated by comma
	</p>
	<?php 
        }
        public function update($new_instance, $old_instance) {
            $instance = array();
            $instance['title'] = ( !empty($new_instance['title']))?strip_tags( $new_instance['title'] ) : '';
            $instance['w_text'] = ( !empty($new_instance['w_text']))?strip_tags( $new_instance['w_text'] ) : '';
            $instance['url'] = ( !empty($new_instance['url']))?strip_tags( $new_instance['url'] ) : '';
            return $instance;
        }
}

function wpb_load_widget(){
    register_widget('text_widget');
    register_widget('text_url_widget');
}
add_action('widgets_init','wpb_load_widget');

/* Stop Adding Functions Below this Line */
?>