<?php get_header(); ?>		
                    
		        <div class="sidebar">
<?php getCatsOfPostType('accessory_categories', 'accessories'); ?>
    <?php get_sidebar('left') ?>
    </div>
			<div class="content">
				<div class="about">
                                    <?php if (have_posts()) : while (have_posts()):the_post(); ?>
                                    <h2 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>
				<?php the_content(); ?>
	<?php endwhile; endif; ?>
				</div>
			</div>
    <?php get_footer(); ?>