<?php get_header(); ?>
<!--div body ended in the footer-->
        <div class="sidebar">
<?php getCatsOfPostType('accessory_categories', 'accessories'); ?>
    <?php get_sidebar('left') ?>
    </div>
    <div class="content">
        <div class="figure">
            <img src="<?php echo get_template_directory_uri(); ?>/images/apparel.jpg" alt=""/>
        </div>
        <?php
        $temp = $wp_query;
//       
 if ( get_query_var('paged') ) { $paged = get_query_var('paged');}
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }
// var_dump($paged);
        $wp_query = new WP_Query(array('post_type' => 'accessories', 'posts_per_page' => 4, 'paged' => $paged));
        ?>

        <div class="products">
            <div class="paging">
                <div class="first">
                    <h2>New Arrival</h2>
                    <span>Show</span>
                    <ul>
                        <li class="selected"><a href="#">8</a></li>
                        <li><a href="#">10</a></li>
                        <li><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                    </ul>
                </div>
                <div>

                    <?php wpbeginner_numeric_posts_nav(); ?>

                </div>
            </div>
            <ul>

                <?php
                $count = 0;
                while ($wp_query->have_posts()) : $wp_query->the_post();
                    ?>
                    <?php if ($count < 4): ?>
                        <?php if ($count == 0): ?>
                            <li class="first">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail() ?></a>
                                <h4><?php the_title(); ?></h4>
                                <p><?php the_content(); ?></p>

                                <span><?php echo get_post_meta($wp_query->post->ID, 'price', TRUE) . ' $'; ?></span>
                            </li>

        <?php else: ?>
                            <li>
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail() ?></a>
                                <h4><?php the_title(); ?></h4>
                                <p><?php the_content(); ?></p>

                                <span><?php echo get_post_meta($wp_query->post->ID, 'price', TRUE) . ' $'; ?></span>
                            </li>
        <?php endif; ?>
                        <?php $count++; ?>
                    <?php else: ?>
                        <?php $count = 1; ?>
                    </ul><ul>

                        <li class="first">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail() ?></a>
                            <h4><?php the_title(); ?></h4>
                            <p><?php the_content(); ?></p>
                            <span><?php echo get_post_meta($wp_query->post->ID, 'price', TRUE) . ' $'; ?></span>
                        </li>
    <?php endif; ?>
                <?php
                endwhile;
                wp_reset_query();
                ?>
            </ul>
        </div>
    </div>
    <?php get_footer(); ?>