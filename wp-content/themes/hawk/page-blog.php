<?php get_header(); ?>		

<div class="sidebar">
    <?php getCatsOfPostType('accessory_categories', 'accessories'); ?>
    <?php get_sidebar('left') ?>
</div>
<?php
$temp = $wp_query;
//       
if (get_query_var('paged')) {
    $paged = get_query_var('paged');
} elseif (get_query_var('page')) {
    $paged = get_query_var('page');
} else {
    $paged = 1;
}
// var_dump($paged);
$wp_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 8, 'paged' => $paged));
//var_dump($wp_query) ;
?>
<div class="content">
    <div class="blog">
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

            <div>
                <div class="stats">
                    <div class="date"><span><?php echo the_date('Md', '', '', FALSE); ?></span></div>

                    <div class="share">



                        <div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
                        <script>
                        </script>
                        <a href="http://twitter.com/fwtemplates" id="tweets"></a>
                        <a href="#" id="heart"></a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.timeanddate.com/timer/&t=<?php the_title(); ?>" id="likes"></a>

                    </div>
                </div>
                <div>
                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    <h2>Check out the latest designs</h2>
                    <?php the_content(); ?>
                    
                </div>
            </div>
            <?php
        endwhile;
//wp_reset_query();
        ?>
        <div class="paging">
            <div>

                <?php wpbeginner_numeric_posts_nav(); ?>

            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>