<?php

register_nav_menu('hii', 'high menu');
register_nav_menu('top', 'top menu');
add_theme_support('post-thumbnails');
function hawk_scripts() {
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-2.1.1.js', array(), '2.1.1', true );
}

add_action( 'wp_enqueue_scripts', 'hawk_scripts' );
function create_apparel() {
    $labels = array(
        'name' => _x('Apparels', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Apparel', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Apparel', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Apparels', 'text_domain'),
        'view_item' => __('View Apparel', 'text_domain'),
        'add_new_item' => __('Add New Apparel', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'edit_item' => __('Edit Apparel', 'text_domain'),
        'update_item' => __('Update Apparel', 'text_domain'),
        'search_items' => __('Search Apparels', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    );
    $args = array(
        'label' => __('Apparels', 'text_domain'),
        'description' => __('Apparel Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        'taxonomies' => array( 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('apparels', $args);
}

// Hook into the 'init' action
add_action('init', 'create_apparel', 0);

function create_shoes() {
    $labels = array(
        'name' => _x('Shoes', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Shoe', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Shoe', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Shoes', 'text_domain'),
        'view_item' => __('View Shoe', 'text_domain'),
        'add_new_item' => __('Add New Shoe', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'edit_item' => __('Edit Shoe', 'text_domain'),
        'update_item' => __('Update Shoe', 'text_domain'),
        'search_items' => __('Search Shoes', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    );
    $args = array(
        'label' => __('Shoes', 'text_domain'),
        'description' => __('Shoe Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        'taxonomies' => array( 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('shoes', $args);
}

// Hook into the 'init' action
add_action('init', 'create_shoes', 0);

function create_accessories() {
    $labels = array(
        'name' => _x('accessories', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('accessory', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('accessory', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All accessories', 'text_domain'),
        'view_item' => __('View accessory', 'text_domain'),
        'add_new_item' => __('Add New accessory', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'edit_item' => __('Edit accessory', 'text_domain'),
        'update_item' => __('Update accessory', 'text_domain'),
        'search_items' => __('Search accessories', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    );
    $args = array(
        'label' => __('accessories', 'text_domain'),
        'description' => __('accessory Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        'taxonomies' => array( 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('accessories', $args);
}

// Hook into the 'init' action
add_action('init', 'create_accessories', 0);

function create_jewelry() {
    $labels = array(
        'name' => _x('jewelry', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('jewelry', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('jewelry', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All jewelry', 'text_domain'),
        'view_item' => __('View jewelry', 'text_domain'),
        'add_new_item' => __('Add New jewelry', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'edit_item' => __('Edit jewelry', 'text_domain'),
        'update_item' => __('Update jewelry', 'text_domain'),
        'search_items' => __('Search jewelry', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    );
    $args = array(
        'label' => __('jewelry', 'text_domain'),
        'description' => __('jewelry Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        'taxonomies' => array( 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('jewelry', $args);
}

// Hook into the 'init' action
add_action('init', 'create_jewelry', 0);

function create_beauty_care() {
    $labels = array(
        'name' => _x('beauty cares', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('beauty care', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('beauty care', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All beauty cares', 'text_domain'),
        'view_item' => __('View beauty care', 'text_domain'),
        'add_new_item' => __('Add New beauty care', 'text_domain'),
        'add_new' => __('Add New', 'text_domain'),
        'edit_item' => __('Edit beauty care', 'text_domain'),
        'update_item' => __('Update beauty care', 'text_domain'),
        'search_items' => __('Search beauty cares', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    );
    $args = array(
        'label' => __('beauty cares', 'text_domain'),
        'description' => __('beauty care Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        'taxonomies' => array( 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('beauty_care', $args);
}

// Hook into the 'init' action
add_action('init', 'create_beauty_care', 0);

//add sidebar
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Left Sidebar',
        'id' => 'left-sidebar',
        'description' => 'Appears as the sidebar on left',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2><a href="#">',
        'after_title' => '</a></h2>',
    ));
    register_sidebar(array(
         'name' => 'Footer Sidebar',
        'id' => 'footer-sidebar',
        'description' => 'Appears as the sidebar on footer',
//        'before_widget' => '<div>',
//        'after_widget' => '</div>',
//        'before_title' => '<h2>',
//        'after_title' => '</h2>',
    ));
}


function wpbeginner_numeric_posts_nav() {

    if (is_singular())
        return;

    global $wp_query;

//    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1)
        return;

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max = intval($wp_query->max_num_pages);
//            var_dump($paged);
    /** Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (( $paged + 2 ) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
    if (get_previous_posts_link()) {
        $pre = $paged - 1;
        echo '<a href="?page=' . $pre . '">pre</a>';
    }
    echo '<ul>' . "\n";

    /** Previous Post Link */
    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url('?page=1'), '1');

        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="selected"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url('?page=' . $link), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url('?page=' . $max), $max);
    }
    echo '</ul>' . "\n";
    /** Next Post Link */
    if (get_next_posts_link()) {
//	        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
        $next = $paged + 1;
        echo '<a href="?page=' . $next . '">next</a>';
    }
}

function themes_taxonomy() {  
    register_taxonomy(  
        'apparel_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'apparels',        //post type name
        array(  
            'hierarchical' => TRUE,  
            'label' => 'Apparels store',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'apparel_categories', // This controls the base slug that will display before each term
                'with_front' => TRUE // Don't display the category base before 
            )
        )  
    );  
    register_taxonomy(  
        'shoe_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'shoes',        //post type name
        array(  
            'hierarchical' => TRUE,  
            'label' => 'Shoes store',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'shoe_categories', // This controls the base slug that will display before each term
                'with_front' => TRUE // Don't display the category base before 
            )
        )  
    );  
    register_taxonomy(  
        'accessory_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'accessories',        //post type name
        array(  
            'hierarchical' => TRUE,  
            'label' => 'Accessories store',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'accessory_categories', // This controls the base slug that will display before each term
                'with_front' => TRUE // Don't display the category base before 
            )
        )  
    );  
    register_taxonomy(  
        'jewelry_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'jewelry',        //post type name
        array(  
            'hierarchical' => TRUE,  
            'label' => 'Jewelry store',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'jewelry_categories', // This controls the base slug that will display before each term
                'with_front' => TRUE // Don't display the category base before 
            )
        )  
    );  
    register_taxonomy(  
        'beauty_care_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'beauty_care',        //post type name
        array(  
            'hierarchical' => TRUE,  
            'label' => 'beauty care store',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'beauty_care_categories', // This controls the base slug that will display before each term
                'with_front' => TRUE // Don't display the category base before 
            )
        )  
    );  
}  
add_action( 'init', 'themes_taxonomy');

function getCatsOfPostType($cat , $type){
    ?>
<div class="first">
    <h2><a href="#">Categories</a></h2>
    <ul>
<?php 
   $args = array(
        'name'=>$cat);
    $output = 'objects';
    $taxonomies=get_categories('taxonomy='.$cat.'&type='.$type); 
    foreach ($taxonomies as $tax){
        ?>
          <li><a href="<?php bloginfo('url') ?><?php echo '/'.$cat .'/' . $tax->slug; ?>/"><?php echo $tax->name; ?></a></li>
        <?php
    }
    ?>
               </ul>
</div>
          <?php
}
